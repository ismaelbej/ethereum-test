export default (value) => {
  return new Promise(resolve => resolve(value.toNumber()));
};
